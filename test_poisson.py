from efit import efit
import matplotlib.pyplot as plt
from mesh.plasmah_mesh_with_down_divertor import *
from plasmah.poisson import *
import os


#efit
test_efit = efit.Efit()
test_efit.read_gfile("efit/g037007.00501.hl2a")
test_efit.init_interp()
test_efit.read_firstwall_divertor("efit/firstwall_inner_hl2a.txt", "efit/firstwall_hl2a.txt", "efit/divertor_hl2a.txt")
test_efit.write_gfile("efit/g037007.00501.hl2a.divertor")
#test_efit.plot_divertor()


#test_efit.plot_sol2d()

#mesh
#mesh_hl2a = PlasamHMesh(test_efit)
#mesh_hl2a.generate_mesh(0.02)

#mesh_hl2a.save_mesh("test_poisson_hl2a.msh")

#convert mesh
os.system('dolfin-convert test_poisson_hl2a.msh test_poisson_hl2a.xml')


#PlasmaHModel
model = PlasmaHModel()
model.run()

#delete mesh
#os.system('rm test_poisson_hl2a.msh test_poisson_hl2a*.xml')
