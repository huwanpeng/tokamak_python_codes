import numpy as np
from scipy import interpolate
import matplotlib.pylab as plt

nx = 5
ny = 4

x = np.linspace(1.0, 5.0, nx)
y = np.linspace(1.0, 4.0, ny)

xx, yy = np.meshgrid(x, y, indexing='ij')

z = np.zeros((nx, ny))
for i in range(0, nx):
    for j in range(0, ny):
        z[i,j] = j * 1.0

print(x, y)
print(xx.shape, yy.shape)
print(xx[4, 1])

interp2d = interpolate.RectBivariateSpline(x, y, z)

a = interp2d(3.5, 1.0, grid=False)
b = interp2d(3.5, 1.0, dx = 1, grid=False)
print(a, b)


fig=plt.figure(figsize=(10,5))
fig.subplots_adjust(top=0.9,bottom=0.1,wspace=0.5,hspace=0.55)

ax0=fig.add_subplot(1,1,1)

ct0 = ax0.contourf(xx, yy, z)


ax0.set_aspect('equal')
plt.show()