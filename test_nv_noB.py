from efit import efit
import matplotlib.pyplot as plt
from mesh.plasmah_mesh_with_down_divertor import *
from plasmah.sol2d_nv_noB import *

#efit
test_efit = efit.Efit()
test_efit.read_gfile("efit/g037007.00501.hl2a")
test_efit.init_interp()
test_efit.read_firstwall_divertor("efit/firstwall_inner_hl2a.txt", "efit/firstwall_hl2a.txt", "efit/divertor_hl2a.txt")
test_efit.write_gfile("efit/g037007.00501.hl2a.divertor")



#test_efit.plot_sol2d()

#mesh
#mesh_hl2a = PlasamHMesh(test_efit)
#mesh_hl2a.generate_mesh(0.02)

#mesh_hl2a.save_mesh("hl2a.msh")

#convert mesh
os.system('dolfin-convert hl2a.msh hl2a.xml')


#PlasmaHModel
model = PlasmaHModel(test_efit)
model.run()

#delete mesh
#os.system('rm hl2a.msh hl2a*.xml')




