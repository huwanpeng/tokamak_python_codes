from efit import efit
import matplotlib.pyplot as plt
import os


#efit
test_efit = efit.Efit()
test_efit.read_gfile("efit/g037007.00501.hl2a")
test_efit.init_interp()
test_efit.read_firstwall_divertor("efit/firstwall_inner_hl2a.txt", "efit/firstwall_hl2a.txt", "efit/divertor_hl2a.txt")
test_efit.write_gfile("efit/g037007.00501.hl2a.divertor")

test_efit.plot_divertor()
test_efit.plot_sol2d()