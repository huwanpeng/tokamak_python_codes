import gmsh
import sys



gmsh.initialize()
gmsh.model.add("gmsh_occ_mesh")

mesh_length = 0.1

#define plane0
a = gmsh.model.occ.addPoint(0, 0, 0, mesh_length)
b = gmsh.model.occ.addPoint(1, 0, 0, mesh_length)
c = gmsh.model.occ.addPoint(1, 1, 0, mesh_length)
d = gmsh.model.occ.addPoint(0, 1, 0, mesh_length)

ab = gmsh.model.occ.addLine(a, b)
bc = gmsh.model.occ.addLine(b, c)
cd = gmsh.model.occ.addLine(c, d)
da = gmsh.model.occ.addLine(d, a)


curve_loop0 = gmsh.model.occ.addCurveLoop([ab, bc, cd, da])

plane0 = gmsh.model.occ.addPlaneSurface([curve_loop0])


#define plane１
a1 = gmsh.model.occ.addPoint(0+0.25, 0+0.25, 0, mesh_length)
b1 = gmsh.model.occ.addPoint(0.5+0.25, 0+0.25, 0, mesh_length)
c1 = gmsh.model.occ.addPoint(0.5+0.25, 0.5+0.25, 0, mesh_length)
d1 = gmsh.model.occ.addPoint(0+0.25, 0.5+0.25, 0, mesh_length)

ab1 = gmsh.model.occ.addLine(a1, b1)
bc1 = gmsh.model.occ.addLine(b1, c1)
cd1 = gmsh.model.occ.addLine(c1, d1)
da1 = gmsh.model.occ.addLine(d1, a1)


curve_loop1 = gmsh.model.occ.addCurveLoop([ab1, bc1, cd1, da1])

plane1 = gmsh.model.occ.addPlaneSurface([curve_loop1])

plane2 = gmsh.model.occ.cut([(2, plane0)], [(2, plane1)])
#gmsh.model.occ.remove([(2, plane1)], recursive = True)

gmsh.model.occ.synchronize()



gmsh.model.mesh.generate(2)



# Launch the GUI to see the results:
if '-nopopup' not in sys.argv:
    gmsh.fltk.run()

gmsh.finalize()
