import gmsh
import sys

class PlasamHMesh:
    def __init__(self, efit):
        self.efit = efit

    def generate_mesh(self, mesh_length = 0.05):
        
        gmsh.initialize()
        gmsh.model.add("gmsh_occ_mesh")

        self.mesh_length = mesh_length

        #define first wall
        self.first_wall_point_list = []
        for i in range(self.efit.wall_all_number):
            if i > 0 and self.efit.wall_all[i, 0] == self.efit.wall_all[i-1, 0] and self.efit.wall_all[i, 1] == self.efit.wall_all[i-1, 1]:
                pass
            elif i == self.efit.wall_all_number - 1 and self.efit.wall_all[i, 0] == self.efit.wall_all[0, 0] and self.efit.wall_all[i, 1] == self.efit.wall_all[0, 1]:
                pass
            else:
                point = gmsh.model.occ.addPoint(self.efit.wall_all[i, 0], self.efit.wall_all[i, 1], 0, self.mesh_length)
                self.first_wall_point_list.append(point)


        self.first_wall_line_list = []
        for i in range(len(self.first_wall_point_list)):
            #print("line list ", self.efit.wall_all[i, 0], self.efit.wall_all[i, 1], self.efit.wall_all[i+1, 0], self.efit.wall_all[i+1, 1])
            if i == len(self.first_wall_point_list) - 1:
                line = gmsh.model.occ.addLine(self.first_wall_point_list[-1], self.first_wall_point_list[0])
            else:
                line = gmsh.model.occ.addLine(self.first_wall_point_list[i], self.first_wall_point_list[i+1])
            self.first_wall_line_list.append(line)
            #print("line list ", i, line)


        self.first_wall_curve_loop = gmsh.model.occ.addCurveLoop(self.first_wall_line_list)

        self.first_wall_plane = gmsh.model.occ.addPlaneSurface([self.first_wall_curve_loop])


        #define inner edge
        self.inner_edge_point_list = []
        for i in range(len(self.efit.inner_edge)):
            if i > 0 and self.efit.inner_edge[i, 0] == self.efit.inner_edge[i-1, 0] and self.efit.inner_edge[i, 1] == self.efit.inner_edge[i-1, 1]:
                pass
            elif i == len(self.efit.inner_edge) - 1 and self.efit.inner_edge[i, 0] == self.efit.inner_edge[0, 0] and self.efit.inner_edge[i, 1] == self.efit.inner_edge[0, 1]:
                pass
            else:
                point = gmsh.model.occ.addPoint(self.efit.inner_edge[i, 0], self.efit.inner_edge[i, 1], 0, self.mesh_length)
                self.inner_edge_point_list.append(point)


        self.inner_edge_line_list = []
        for i in range(len(self.inner_edge_point_list)):
            #print("line list ", self.efit.inner_edge[i, 0], self.efit.inner_edge[i, 1], self.efit.inner_edge[i+1, 0], self.efit.inner_edge[i+1, 1])
            if i == len(self.inner_edge_point_list) - 1:
                line = gmsh.model.occ.addLine(self.inner_edge_point_list[-1], self.inner_edge_point_list[0])
            else:
                line = gmsh.model.occ.addLine(self.inner_edge_point_list[i], self.inner_edge_point_list[i+1])
            self.inner_edge_line_list.append(line)
            #print("line list ", i, line)


        self.inner_edge_curve_loop = gmsh.model.occ.addCurveLoop(self.inner_edge_line_list)

        self.inner_edge_plane = gmsh.model.occ.addPlaneSurface([self.inner_edge_curve_loop])

        #cut
        #self.sim_plane = gmsh.model.occ.cut([(2, self.first_wall_plane)], [(2, self.inner_edge_plane)])
        self.sim_plane = gmsh.model.occ.fragment([(2, self.first_wall_plane)], [(2, self.inner_edge_plane)])
        gmsh.model.occ.remove([(2, self.inner_edge_plane)], recursive = True)
        print("self.sim_plane ", self.sim_plane)
        

        gmsh.model.occ.synchronize()

        print("self.first_wall_line_list ", self.first_wall_line_list)

        #define physical group
        self.first_wall_physical_group = gmsh.model.addPhysicalGroup(1, self.first_wall_line_list)
        self.inner_edge_physical_group = gmsh.model.addPhysicalGroup(1, self.inner_edge_line_list)
        #self.sim_plane_physical_group = gmsh.model.addPhysicalGroup(2, [self.sim_plane])
        self.sim_plane_physical_group = gmsh.model.addPhysicalGroup(2, [3])

        gmsh.model.setPhysicalName(1, self.first_wall_physical_group, "first_wall")
        gmsh.model.setPhysicalName(1, self.inner_edge_physical_group, "inner_edge")
        gmsh.model.setPhysicalName(2, self.sim_plane_physical_group, "sim_plane")

        gmsh.model.occ.synchronize()
        gmsh.model.mesh.generate(2)




        #write mesh
        #gmsh.option.setNumber("Mesh.SaveAll", 1)
        gmsh.option.setNumber("Mesh.MshFileVersion", 2.2)
        #gmsh.write("hl2a.msh")

        # Launch the GUI to see the results:
        if '-nopopup' not in sys.argv:
            gmsh.fltk.run()

    def save_mesh(self, mesh_name = "hl2a.msh"):
        #gmsh.write("model.brep")
        gmsh.write(mesh_name)
        gmsh.finalize()
