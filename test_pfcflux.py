from efit import efit
import matplotlib.pyplot as plt
from mesh.plasmah_mesh import *
from plasmah.poisson import *
import os


#efit
prefix = "data_pfcflux"
test_efit = efit.Efit()
test_efit.read_gfile(prefix + "/gfile_EFIT")
test_efit.init_interp()
test_efit.read_firstwall_divertor(prefix + "/firstwall_inner_hl2a.txt", prefix + "/firstwall_hl2a.txt", prefix + "/divertor_hl2a.txt")
test_efit.write_gfile(prefix + "/g037007.00501.hl2a.divertor")

test_efit.read_flux_wall(prefix + "/cfetr_wall_out.txt")
#test_efit.read_flux_wall(prefix + "/cfetr_wall_line.txt")
#test_efit.read_flux_wall(prefix + "/cfetr_wall_inner.txt")

test_efit.cal_flux()

test_efit.plot_sol2d()

